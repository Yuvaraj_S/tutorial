// public class MyFirstJavaProgram{  
  
//    /* This is my first java program. 
//     * This will print 'Hello World' as the output 
//     * This is an example of multi-line comments. 
//     */  
  
//     public static void main(String []args){  
//        // This is an example of single line comment  
//        /* This is also an example of single line comment. */  
//        System.out.println("Hello World");   
//     }  
class ContinueWithLabelDemo {  

    public static void main(String[] args) {  
  
        String searchMe = "Look for a substring in me";  
        String substring = "u";  
        boolean foundIt = false;  
  
        int max = searchMe.length() -   
                  substring.length();  
  
    test:  
        for (int i = 0; i <= max; i++) {  
            int n = substring.length();  
            // int j = i;  
            int k = 0;  
            while (n-- != 0) {  
                if (searchMe.charAt(i++) != substring.charAt(k++)) {  
                    continue test;  
                }  
            }  
            foundIt = true;  
                break test;  
        }  
        System.out.println(foundIt ? "Found it" : "Didn't find it");  
    }  
}  